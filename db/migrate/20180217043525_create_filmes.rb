class CreateFilmes < ActiveRecord::Migration[5.1]
  def change
    create_table :filmes do |t|
      t.string :title
      t.string :originalTitle
      t.string :slug
      t.text :sinopse
      t.integer :duration
      t.integer :likes
      t.boolean :isPublished

      t.timestamps
    end
  end
end
