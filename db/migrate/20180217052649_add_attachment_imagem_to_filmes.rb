class AddAttachmentImagemToFilmes < ActiveRecord::Migration[5.1]
  def self.up
    change_table :filmes do |t|
      t.attachment :imagem
    end
  end

  def self.down
    remove_attachment :filmes, :imagem
  end
end
