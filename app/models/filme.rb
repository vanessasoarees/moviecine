class Filme < ApplicationRecord

  has_attached_file :imagem, styles: { medium: "6000x400", thumb: "200x200" }
  validates_attachment_content_type :imagem, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

end
