class FilmesController < ApplicationController

  before_action :authenticate_user!, :except=>[:show, :index]

  def index
    @filmes = Filme.all
  end

  def show

    @filme = Filme.find(params[:id])

  end


 def create

      @filme = Filme.new(filmes_params)

   if @filme.save
     redirect_to @filme
   else
     render 'new'
   end
 end


 private
   def filmes_params
     params.require(:filmes).permit(:title, :originalTitle, :slug, :sinopse, :duration, :likes, :isPublished, :imagem, :listaAtores)
   end

end
