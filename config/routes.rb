Rails.application.routes.draw do


  devise_for :users do
    get '/users/sign_out' => 'devise/sessions#destroy'
  end
  get 'usuarios/new'

  resources :filmes

  root :to => "filmes#index"

end
